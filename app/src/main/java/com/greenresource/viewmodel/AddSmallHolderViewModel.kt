package com.greenresource.viewmodel

import android.app.Application
import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.greenresource.model.ModelSignUp
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.utils.DataManager
import com.greenresource.utils.util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddSmallHolderViewModel(application: Application) : AndroidViewModel(application),
    Callback<ModelSignUp> {
    var firstName: ObservableField<String>? = null
    var lastName: ObservableField<String>? = null
    var email: ObservableField<String>? = null
    var userAddress: ObservableField<String>? = null
    var userLatitude: ObservableField<String>? = null
    var userLongitude: ObservableField<String>? = null
    var btnSelected: ObservableBoolean? = null
    var progressDialog: MutableLiveData<Boolean>? = null
    var modelSignUp: MutableLiveData<ModelSignUp>? = null


    init {
        firstName = ObservableField("")
        lastName = ObservableField("")
        email = ObservableField("")
        userAddress = ObservableField("")
        userLatitude = ObservableField("")
        userLongitude = ObservableField("")
        btnSelected = ObservableBoolean(false)
        progressDialog = MutableLiveData()
        modelSignUp = MutableLiveData()
    }

    fun onSetLocation(userAddress1: String, latitude: String, longitude: String) {
        userAddress?.set(userAddress1)
        userLatitude?.set(latitude)
        userLongitude?.set(longitude)
    }


    fun onChangeFirstName(char: CharSequence, start: Int, end: Int, count: Int) {
        firstName?.set(char.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())
        )
    }

    fun onChangeLastName(char: CharSequence, start: Int, end: Int, count: Int) {
        lastName?.set(char.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!!
        )
    }

    fun onChangeEmail(char: CharSequence, start: Int, end: Int, count: Int) {
        email?.set(char.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())
        )
    }

    fun onAddSmallHolder(context: Context) {
        progressDialog?.value = true
        var map = HashMap<String, String>()
        map["first_name"] = firstName?.get()!!
        map["user_id"] = DataManager.getInstance()?.getUserInfo(context)?.userinfo?.id!!
        map["last_name"] = lastName?.get()!!
        map["email"] = email?.get()!!
        map["ios_token"] = ""
        map["android_token"] = ""
        map["user_latitude"] = userLatitude?.get()!!
        map["user_longitude"] = userLongitude?.get()!!
        map["user_address"] = userAddress?.get()!!
        WebServiceClient?.client?.create(BackEndApi::class.java).addSmallholders(map).enqueue(this)
    }

    override fun onFailure(call: Call<ModelSignUp>?, t: Throwable?) {
        progressDialog?.value = false
    }

    override fun onResponse(call: Call<ModelSignUp>?, response: Response<ModelSignUp>?) {
        progressDialog?.value = false
        if (response?.code() == 200) {
            modelSignUp?.value = response?.body()
        } else {

        }
    }

}