package com.greenresource.viewmodel

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.greenresource.model.ModelLogin
import com.greenresource.model.ModelSignUp
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.utils.util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel(application: Application) : AndroidViewModel(application),
    Callback<ModelLogin> {
    var email: ObservableField<String>? = null
    var password: ObservableField<String>? = null
    var btnSelected: ObservableBoolean? = null
    var progressDialog: MutableLiveData<Boolean>? = null
    var modelLogin: MutableLiveData<ModelLogin>? = null

    init {
        email = ObservableField("")
        password = ObservableField("")
        btnSelected = ObservableBoolean(false)
        progressDialog = MutableLiveData()
        modelLogin = MutableLiveData()
    }


    fun onChangeEmail(char: CharSequence, start: Int, end: Int, count: Int) {
        email?.set(char.toString())
        btnSelected?.set(
            email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!! && password?.get()
                ?.isNotEmpty()!!
        )
    }

    fun onChangePassword(char: CharSequence, start: Int, end: Int, count: Int) {
        password?.set(char.toString())
        btnSelected?.set(
            email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!! && password?.get()
                ?.isNotEmpty()!!
        )
    }

    fun onLogin() {
        progressDialog?.value = true
        var map = HashMap<String, String>()
        map.put("email", email?.get()!!)
        map.put("password", password?.get()!!)
        map.put("ios_token", "")
        map.put("android_token", FirebaseInstanceId.getInstance()?.token!!)
        WebServiceClient?.client?.create(BackEndApi::class.java).login(map).enqueue(this)
    }

    override fun onFailure(call: Call<ModelLogin>?, t: Throwable?) {
        progressDialog?.value = false
    }

    override fun onResponse(call: Call<ModelLogin>?, response: Response<ModelLogin>?) {
        progressDialog?.value = false
        if (response?.code() == 200) {
            modelLogin?.value = response?.body()
        } else {

        }
    }

}