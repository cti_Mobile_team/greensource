package com.greenresource.viewmodel

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.firebase.iid.FirebaseInstanceId
import com.greenresource.model.ModelSignUp
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.utils.Constant
import com.greenresource.utils.util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUpViewModel(application: Application) : AndroidViewModel(application),
    Callback<ModelSignUp> {
    var firstName: ObservableField<String>? = null
    var lastName: ObservableField<String>? = null
    var email: ObservableField<String>? = null
    var categorySelection: ObservableField<String>? = null
    var password: ObservableField<String>? = null
    var confirmPassword: ObservableField<String>? = null
    var userAddress: ObservableField<String>? = null
    var userLatitude: ObservableField<String>? = null
    var userLongitude: ObservableField<String>? = null
    var btnSelected: ObservableBoolean? = null
    var progressDialog: MutableLiveData<Boolean>? = null
    var modelSignUp: MutableLiveData<ModelSignUp>? = null

    init {
        firstName = ObservableField("")
        lastName = ObservableField("")
        email = ObservableField("")
        categorySelection = ObservableField("")
        password = ObservableField("")
        confirmPassword = ObservableField("")
        userAddress = ObservableField("")
        userLatitude = ObservableField("")
        userLongitude = ObservableField("")
        btnSelected = ObservableBoolean(false)
        progressDialog = MutableLiveData()
        modelSignUp = MutableLiveData()
    }

    fun onSetLocation(userAddress1: String, latitude: String, longitude: String) {
        Log.e("TAG Location", latitude + longitude)
        userAddress?.set(userAddress1)
        userLatitude?.set(latitude)
        userLongitude?.set(longitude)
    }

    fun onChangeFirstName(char: CharSequence, start: Int, end: Int, count: Int) {
        firstName?.set(char.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!!
                    && categorySelection?.get()?.isNotEmpty()!! && password?.get()
                ?.isNotEmpty()!! && confirmPassword?.get()?.isNotEmpty()!! && password?.get()
                ?.equals(confirmPassword?.get())!!
        )
    }

    fun onChangeLastName(char: CharSequence, start: Int, end: Int, count: Int) {
        lastName?.set(char.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!!
                    && categorySelection?.get()?.isNotEmpty()!! && password?.get()
                ?.isNotEmpty()!! && confirmPassword?.get()?.isNotEmpty()!! && password?.get()
                ?.equals(confirmPassword?.get())!!
        )
    }

    fun onChangeEmail(char: CharSequence, start: Int, end: Int, count: Int) {
        email?.set(char.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!!
                    && categorySelection?.get()?.isNotEmpty()!! && password?.get()
                ?.isNotEmpty()!! && confirmPassword?.get()?.isNotEmpty()!! && password?.get()
                ?.equals(confirmPassword?.get())!!
        )
    }

    fun onChangePassword(char: CharSequence, start: Int, end: Int, count: Int) {
        password?.set(char.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!!
                    && categorySelection?.get()?.isNotEmpty()!! && password?.get()
                ?.isNotEmpty()!! && confirmPassword?.get()?.isNotEmpty()!! && password?.get()
                ?.equals(confirmPassword?.get())!!
        )
    }

    fun onChangeConfirmPassword(char: CharSequence, start: Int, end: Int, count: Int) {
        confirmPassword?.set(char.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!!
                    && categorySelection?.get()?.isNotEmpty()!! && password?.get()
                ?.isNotEmpty()!! && confirmPassword?.get()?.isNotEmpty()!! && password?.get()
                ?.equals(confirmPassword?.get())!!
        )
    }

    fun onSelectItem(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        if (position == 0) {
            categorySelection?.set(Constant.employee)
        } else if (position == 1) {
            categorySelection?.set(Constant.smallHolders)
        } else if (position == 2) {
            categorySelection?.set(Constant.buyer)
        }
        // categorySelection?.set(parent?.selectedItem?.toString())
        btnSelected?.set(
            firstName?.get()?.isNotEmpty()!! && lastName?.get()?.isNotEmpty()!! && email?.get()
                ?.isNotEmpty()!!
                    && util.isEmailValid(email?.get().toString())!!
                    && categorySelection?.get()?.isNotEmpty()!! && password?.get()
                ?.isNotEmpty()!! && confirmPassword?.get()?.isNotEmpty()!! && password?.get()
                ?.equals(confirmPassword?.get())!!
        )
    }

    fun onSignUp() {
        progressDialog?.value = true
        var map = HashMap<String, String>()
        map.put("first_name", firstName?.get()!!)
        map.put("last_name", lastName?.get()!!)
        map.put("email", email?.get()!!)
        map.put("password", password?.get()!!)
        map.put("category_id", categorySelection?.get()!!)
        map.put("ios_token", "")
        map.put("android_token", FirebaseInstanceId.getInstance()?.token!!)
        map.put("device_type", "1")
        map.put("profile_image", "")
        map.put("user_latitude", userLatitude?.get()!!)
        map.put("user_longitude", userLongitude?.get()!!)
        map.put("user_address", userAddress?.get()!!)
        // Log.e("TAG", map.toString())
        WebServiceClient?.client?.create(BackEndApi::class.java).signUp(map).enqueue(this)
    }

    override fun onFailure(call: Call<ModelSignUp>?, t: Throwable?) {
        progressDialog?.value = false
    }

    override fun onResponse(call: Call<ModelSignUp>?, response: Response<ModelSignUp>?) {
        progressDialog?.value = false
        if (response?.code() == 200) {
            modelSignUp?.value = response?.body()
        } else {

        }
    }
}