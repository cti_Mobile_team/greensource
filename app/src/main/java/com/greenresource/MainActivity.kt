package com.greenresource

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.ClusterManager.OnClusterItemClickListener
import com.greenresource.greensourceemployee.NotificationActivity
import com.greenresource.greensourceemployee.adapter.NavigationAdapter
import com.greenresource.model.ModelLogin
import com.greenresource.model.ModelNearBy
import com.greenresource.model.ModelSmallHolderList
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.smallholders.dialog.DialogEntry
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.utils.DataManager
import com.greenresource.utils.MenuArrayList
import com.greenresource.utils.RenderClustering
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), OnMapReadyCallback,
    NavigationAdapter.SetOnNavigationItemClickListener, DialogEntry.SetonItemSelected {
    private lateinit var mMap: GoogleMap
    val PERMISSIONID = 123
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    var customProgressDialog: CustomDialogProgress? = null

    // var markerOptions = MarkerOptions()
    var userType: String = ""
    val PERMISSION_ID = 42
    var modelLogin: ModelLogin? = null
    var placesClient: PlacesClient? = null
    private lateinit var clusterManager: ClusterManager<ModelNearBy.SmallholdersList>
    private val AUTOCOMPLETE_REQUEST_CODE = 1
    /*  var socketConnection = SocketConnection()
      var socket: Socket? = null*/


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //  userType = intent.getStringExtra("userType")
        userType = DataManager.getInstance().getUserInfo(this).userinfo.categoryId
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        customProgressDialog = CustomDialogProgress(this)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        drawer_layout.setOnClickListener {
            drawer_layout.openDrawer(drawer_layout)
        }
        Places.initialize(
            applicationContext,
            "AIzaSyBHdGhD9IcGYsHtzc5RSRXdFRBlvfayUH4"
        )//    AIzaSyBHdGhD9IcGYsHtzc5RSRXdFRBlvfayUH4
        placesClient = Places.createClient(this)
        image_menu.setOnClickListener {
            drawer_layout.openDrawer(Gravity.LEFT)
        }
        image_notification.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
        }

        tv_current_location.setOnClickListener {
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
            // Start the autocomplete intent.
            val intent =
                Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                    .build(this)///
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
        }

        if (userType == "1") {
            navigation.adapter =
                NavigationAdapter(this, MenuArrayList().employeeMenuList, userType, this)
        } else if (userType == "2") {
            navigation.adapter =
                NavigationAdapter(this, MenuArrayList().smallHolderMenuList, userType, this)
        } else {
            navigation.adapter =
                NavigationAdapter(this, MenuArrayList().buyerMenuList, userType, this)
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        val success = mMap.setMapStyle(
            MapStyleOptions(
                resources
                    .getString(R.string.style_json)
            )
        )
        getLastLocation()
        /* mMap.setOnCameraIdleListener {
             val midLatLng: LatLng = googleMap.cameraPosition.target
             tv_current_location
                 .text = getAddress(
                 midLatLng.latitude,
                 midLatLng.longitude
             )
         }*/

    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), PERMISSIONID
        )
    }

    fun getAddress(latitude: Double?, longitude: Double?): String? {
        Log.e("TAG", "Get last Known Location")
        var address: String? = ""
        try {
            val addresses: List<Address>
            val geocoder = Geocoder(this, Locale.getDefault())
            addresses = geocoder.getFromLocation(latitude!!, longitude!!, 1)
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses[0].getAddressLine(0)
            val city: String = addresses[0].getLocality()
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            /*
              val state: String = addresses[0].getAdminArea()
              val country: String = addresses[0].getCountryName()
              val postalCode: String = addresses[0].getPostalCode()
              val knownName: String = addresses[0].getFeatureName()*/
        } catch (e: Exception) {
            e.printStackTrace()
            getLastLocation()
        }
        tv_current_location.text = address
        return address
    }


    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            //  mMap.clear()
            getNearSmallHolders()
            addmarker(mLastLocation?.latitude, mLastLocation?.longitude!!)
            tv_current_location.text = getAddress(
                mLastLocation?.latitude, mLastLocation?.longitude!!
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }


    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        getNearSmallHolders()
                        // mMap.clear()
                        addmarker(location.latitude, location.longitude)
                        tv_current_location.text = getAddress(
                            location?.latitude, location?.longitude!!
                        )
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }


    private fun addmarker(latitude: Double, longitude: Double) {
        mMap.clear()
        mMap.addMarker(
            MarkerOptions().position(
                LatLng(
                    latitude,
                    longitude
                )
            ).icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_location))
        )
        mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    latitude,
                    longitude
                ), 10f
            )
        )
    }

    override fun onNavigationItemClick(value: String) {
        when (value) {
            "Rent machine" -> {
                DialogEntry(this, this).dialogWithInput("Machine Request", "Quantity")
            }
            "Sell Palm Oil" -> {
                DialogEntry(this, this).dialogWithInput("Sell Request", "Weight")
            }
            "Purchase Palm Oil" -> {
                DialogEntry(this, this).dialogWithInput("Palm Oil Request", "Weight")
            }
        }
    }

    override fun onItemSelection(value: String, title: String) {
        when (title) {
            "Machine Request" -> {
                rentMachineRequest(value)
            }
            "Sell Request" -> {
                sellPalmOilRequest(value)
            }
            "Palm Oil Request" -> {
                palmOilRequest(value)
            }
        }
    }

    private fun sellPalmOilRequest(value: String) {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["user_id"] = DataManager.getInstance().getUserInfo(this).userinfo.id
        map["weight"] = value
        WebServiceClient.client.create(BackEndApi::class.java).sellPalmOilRequest(map)
            .enqueue(object : Callback<ModelSmallHolderList> {
                override fun onFailure(call: Call<ModelSmallHolderList>?, t: Throwable?) {
                    customProgressDialog?.dismiss()
                }

                override fun onResponse(
                    call: Call<ModelSmallHolderList>?,
                    response: Response<ModelSmallHolderList>?
                ) {
                    customProgressDialog?.dismiss()
                    if (response?.code() == 200) {
                        Toast.makeText(
                            this@MainActivity,
                            response?.body()?.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })
    }

    private fun palmOilRequest(value: String) {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["user_id"] = DataManager.getInstance().getUserInfo(this).userinfo.id
        map["weight"] = value
        WebServiceClient.client.create(BackEndApi::class.java).palmOilRequest(map)
            .enqueue(object : Callback<ModelSmallHolderList> {
                override fun onFailure(call: Call<ModelSmallHolderList>?, t: Throwable?) {
                    customProgressDialog?.dismiss()
                }

                override fun onResponse(
                    call: Call<ModelSmallHolderList>?,
                    response: Response<ModelSmallHolderList>?
                ) {
                    customProgressDialog?.dismiss()
                    if (response?.code() == 200) {
                        Toast.makeText(
                            this@MainActivity,
                            response?.body()?.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })
    }

    private fun rentMachineRequest(value: String) {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["user_id"] = DataManager.getInstance().getUserInfo(this).userinfo.id
        map["quantity"] = value
        WebServiceClient.client.create(BackEndApi::class.java).rentMachineRequest(map)
            .enqueue(object : Callback<ModelSmallHolderList> {
                override fun onFailure(call: Call<ModelSmallHolderList>?, t: Throwable?) {
                    customProgressDialog?.dismiss()
                }

                override fun onResponse(
                    call: Call<ModelSmallHolderList>?,
                    response: Response<ModelSmallHolderList>?
                ) {
                    customProgressDialog?.dismiss()
                    if (response?.code() == 200) {
                        Toast.makeText(
                            this@MainActivity,
                            response?.body()?.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })
    }

    fun getNearSmallHolders() {
        customProgressDialog?.show()
        var hashMap = HashMap<String, String>()
        hashMap["user_id"] = DataManager.getInstance().getUserInfo(this).userinfo.id
        WebServiceClient.client.create(BackEndApi::class.java).nearBySmallholders(hashMap)
            .enqueue(object : Callback<ModelNearBy> {
                override fun onFailure(call: Call<ModelNearBy>?, t: Throwable?) {
                    customProgressDialog?.dismiss()
                    Log.e("OnFailure", t?.message)
                }

                override fun onResponse(
                    call: Call<ModelNearBy>?,
                    response: Response<ModelNearBy>?
                ) {
                    customProgressDialog?.dismiss()
                    if (response?.code() == 200) {
                        if (response?.body()?.smallholdersList?.size!! > 0) {
                            setUpCluster(response?.body()?.smallholdersList!!)
                        }

                    }
                }

            })
    }

    private fun setUpCluster(smallholdersList: MutableList<ModelNearBy.SmallholdersList>) {
        clusterManager = ClusterManager<ModelNearBy.SmallholdersList>(this, mMap)
        val venueMarkerRender = RenderClustering(this@MainActivity, mMap, clusterManager)
        clusterManager.renderer = venueMarkerRender
        mMap.setOnCameraIdleListener(clusterManager)
        mMap.setOnMarkerClickListener(clusterManager)
        clusterManager.setOnClusterItemClickListener(OnClusterItemClickListener<ModelNearBy.SmallholdersList?> {
            true
        })
        addItems(smallholdersList)
    }

    private fun addItems(logsList: List<ModelNearBy.SmallholdersList>) {
        for (i in logsList.indices) {
            clusterManager.addItem(logsList[i])
        }
        val builder = LatLngBounds.Builder()
        builder.include(logsList[0].getPosition())
            .include(logsList[logsList.size - 1].getPosition())
        // mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 10))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    // data?.let {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    //  getNearSmallHolders()
                    addmarker(place!!.latLng!!.latitude, place!!.latLng!!.longitude)
                    tv_current_location.text = getAddress(
                        place!!.latLng!!.latitude, place!!.latLng!!.longitude
                    )
                    //}
                }
            }
        }
    }


}