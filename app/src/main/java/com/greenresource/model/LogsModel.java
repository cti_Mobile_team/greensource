package com.greenresource.model;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class LogsModel implements ClusterItem {
    Double lat;
    Double lng;
    String title;

    public LogsModel(Double lat, Double lng, String title, String snippet) {
        this.lat = lat;
        this.lng = lng;
        this.title = title;
        this.snippet = snippet;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    @NonNull
    @Override
    public LatLng getPosition() {
        return new LatLng(lat, lng);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    String snippet;

}


