package com.greenresource.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ModelBuyerList {

    @SerializedName("success")
    @Expose
    public String success;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("buyers_list")
    @Expose
    public List<BuyersList> buyersList = null;

    public class BuyersList {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("password")
        @Expose
        public String password;
        @SerializedName("phone_number")
        @Expose
        public String phoneNumber;
        @SerializedName("category_id")
        @Expose
        public String categoryId;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("city")
        @Expose
        public String city;
        @SerializedName("po_box")
        @Expose
        public String poBox;
        @SerializedName("state")
        @Expose
        public String state;
        @SerializedName("country_id")
        @Expose
        public String countryId;
        @SerializedName("profile_image")
        @Expose
        public String profileImage;
        @SerializedName("social_id")
        @Expose
        public String socialId;
        @SerializedName("ios_token")
        @Expose
        public String iosToken;
        @SerializedName("android_token")
        @Expose
        public String androidToken;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("verify")
        @Expose
        public String verify;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("token")
        @Expose
        public String token;
        @SerializedName("user_type")
        @Expose
        public String userType;
        @SerializedName("remember")
        @Expose
        public String remember;
        @SerializedName("otp")
        @Expose
        public String otp;
        @SerializedName("user_latitude")
        @Expose
        public String userLatitude;
        @SerializedName("user_longitude")
        @Expose
        public String userLongitude;
        @SerializedName("user_address")
        @Expose
        public String userAddress;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("smallholders_pass")
        @Expose
        public String smallholdersPass;
        @SerializedName("weight")
        @Expose
        public String weight;
        @SerializedName("request_id")
        @Expose
        public String requestId;

    }


}
