package com.greenresource.smallholders.dialog

import android.app.Dialog
import android.content.Context
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.greenresource.R

class DialogEntry {
    var context: Context? = null
    var setonItemSelected: SetonItemSelected? = null

    constructor(context: Context, setonItemSelected: SetonItemSelected) {
        this.context = context
        this.setonItemSelected = setonItemSelected
    }

    fun dialogWithInput(title: String, hint: String) {
        var dialog = Dialog(context!!)
        if (title == "Forgot Password") {
            dialog.setContentView(R.layout.dialog_input_email)
        } else {
            dialog.setContentView(R.layout.dialog_input)
        }
        var tv_title: TextView = dialog.findViewById(R.id.tv_title)
        var input_value: EditText = dialog.findViewById(R.id.input_value)
        var btn_send: Button = dialog.findViewById(R.id.btn_send)
        var btn_cancel: Button = dialog.findViewById(R.id.btn_cancel)
        tv_title.text = title
        input_value.hint = hint
        btn_cancel.setOnClickListener {
            dialog?.dismiss()
        }
        btn_send.setOnClickListener {
            if (input_value?.text?.length!! > 0) {
                setonItemSelected?.onItemSelection(input_value?.text?.toString()!!, title)
                dialog.dismiss()
            } else {
                input_value.error = "Field is Required"
            }
        }
        dialog.show()
    }


    interface SetonItemSelected {
        fun onItemSelection(value: String, title: String)
    }
}