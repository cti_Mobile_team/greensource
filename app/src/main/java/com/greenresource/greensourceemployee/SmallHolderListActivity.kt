package com.greenresource.greensourceemployee

import android.content.Intent
import android.os.Bundle
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.greenresource.R
import com.greenresource.model.ModelSmallHolderList
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.greensourceemployee.adapter.AdapterSmallHolder
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.utils.DataManager
import kotlinx.android.synthetic.main.activity_small_holder_list.*
import kotlinx.android.synthetic.main.toolbar_back.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SmallHolderListActivity : AppCompatActivity(), Callback<ModelSmallHolderList> {
    var customProgressDialog: CustomDialogProgress? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_small_holder_list)
        customProgressDialog = CustomDialogProgress(this)
        image_search.visibility = VISIBLE
        tv_title.text = getString(R.string.small_holders)

        image_back.setOnClickListener {
            onBackPressed()
        }
        getSmallHoldersList()

        add_btn.setOnClickListener {
            startActivity(Intent(this, AddSmallHoldersActivity::class.java))
        }

    }

    fun getSmallHoldersList() {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["user_id"] = DataManager.getInstance()?.getUserInfo(this)?.userinfo?.id!!
        WebServiceClient?.client?.create(BackEndApi::class.java).getAllSmallholders(map)
            .enqueue(this)
    }

    override fun onFailure(call: Call<ModelSmallHolderList>?, t: Throwable?) {
        customProgressDialog?.dismiss()
        Toast.makeText(this, t?.message, Toast.LENGTH_SHORT).show()
    }

    override fun onResponse(
        call: Call<ModelSmallHolderList>?,
        response: Response<ModelSmallHolderList>?
    ) {
        customProgressDialog?.dismiss()
        if (response?.code() == 200) {
            if (response?.body()?.success == "1") {
                smallholderlist.adapter = AdapterSmallHolder(
                    this,
                    response?.body()?.smallholders as ArrayList<ModelSmallHolderList.Smallholder>
                )
            }
        }

    }
}