package com.greenresource.greensourceemployee

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import com.google.gson.Gson
import com.greenresource.R
import com.greenresource.model.ModelLogin
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.utils.Constant
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.utils.DataManager
import com.greenresource.utils.SessionManager
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.toolbar_back.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SettingActivity : AppCompatActivity() {
    var customProgressDialog: CustomDialogProgress? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)


        tv_title.text = getString(R.string.settings)

        if (DataManager.getInstance().getUserInfo(this).userinfo.isNotification!! == "1") {
            noti_switch_compact.isChecked = true
        }

        if (DataManager.getInstance().getUserInfo(this).userinfo.isPopups!! == "1") {
            pop_switch_compact.isChecked = true
        }


        if (DataManager.getInstance().getUserInfo(this).userinfo.isOrderhistory!! == "1") {
            order_switch_compact.isChecked = true
        }

        noti_switch_compact?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                updateNotification("1", "is_notification")
            } else {
                updateNotification("0", "is_notification")
            }
        }
        pop_switch_compact?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                updateNotification("1", "is_popups")
            } else {
                updateNotification("0", "is_popups")
            }
        }
        order_switch_compact?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                updateNotification("1", "is_orderhistory")
            } else {
                updateNotification("0", "is_orderhistory")
            }
        }

        image_back.setOnClickListener {
            onBackPressed()
        }
    }

    fun updateNotification(value: String, updateValue: String) {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        if (updateValue.equals("is_popups")) {
            map["is_popups"] = value
        }
        if (updateValue.equals("is_notification")) {
            map["is_notification"] = value
        }
        if (updateValue.equals("is_orderhistory")) {
            map["is_orderhistory"] = value
        }
        map["user_id"] = DataManager.getInstance().getUserInfo(this).userinfo.id
        WebServiceClient.client.create(BackEndApi::class.java).updateNotification(map)
            .enqueue(
                object : Callback<ModelLogin> {
                    override fun onFailure(call: Call<ModelLogin>?, t: Throwable?) {
                        customProgressDialog?.dismiss()
                    }

                    override fun onResponse(
                        call: Call<ModelLogin>?,
                        response: Response<ModelLogin>?
                    ) {
                        customProgressDialog?.dismiss()
                        if (response!!.code() == 200) {

                        }
                    }

                }
            )
    }
}

