package com.greenresource.greensourceemployee

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.greenresource.R
import com.greenresource.greensourceemployee.adapter.AdapterRentMachine
import com.greenresource.model.ModelSmallHolderList
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.utils.DataManager
import kotlinx.android.synthetic.main.activity_rent_machine.*
import kotlinx.android.synthetic.main.toolbar_back.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RentMachineActivity : AppCompatActivity() {
    var customProgressDialog: CustomDialogProgress? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rent_machine)

        image_search.visibility = View.VISIBLE
        tv_title.text = getString(R.string.rent_machine)

        image_back.setOnClickListener {
            onBackPressed()
        }

        customProgressDialog = CustomDialogProgress(this)

        getAllRentMachine()

        image_search.setOnQueryTextFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                tv_title.visibility = View.GONE
            } else {
                tv_title.visibility = View.VISIBLE
            }
        }
    }


    fun getAllRentMachine() {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["user_id"] = DataManager.getInstance().getUserInfo(this).userinfo.id
        WebServiceClient.client.create(BackEndApi::class.java).allrentmachine(map)
            .enqueue(object : Callback<ModelSmallHolderList> {
                override fun onFailure(call: Call<ModelSmallHolderList>?, t: Throwable?) {
                    customProgressDialog?.dismiss()
                }

                override fun onResponse(
                    call: Call<ModelSmallHolderList>?,
                    response: Response<ModelSmallHolderList>?
                ) {
                    customProgressDialog?.dismiss()
                    if (response?.code() == 200) {
                        if (response?.body()?.success == "1") {
                            tv_no_data_found.visibility = View.VISIBLE
                            rv_rent_machine_list.adapter =
                                AdapterRentMachine(this@RentMachineActivity)
                        } else {
                            tv_no_data_found.visibility = View.VISIBLE
                        }
                    } else {
                        tv_no_data_found.visibility = View.VISIBLE
                        Toast.makeText(
                            this@RentMachineActivity,
                            response?.message(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })
    }
}