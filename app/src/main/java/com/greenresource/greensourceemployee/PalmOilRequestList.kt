package com.greenresource.greensourceemployee

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.material.tabs.TabLayout
import com.greenresource.R
import com.greenresource.greensourceemployee.fragment.BuyerFragment
import com.greenresource.greensourceemployee.fragment.FragmentSmallHolder
import kotlinx.android.synthetic.main.activity_palm_oil_request_list.*
import kotlinx.android.synthetic.main.toolbar_back.*

class PalmOilRequestList : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_palm_oil_request_list)

        image_search.visibility = View.VISIBLE
        tv_title.text = getString(R.string.palm_oil_request)

        image_back.setOnClickListener {
            onBackPressed()
        }

        tab_layout!!.addTab(tab_layout!!.newTab()!!.setText("SmallHolder"))
        tab_layout!!.addTab(tab_layout!!.newTab()!!.setText("Buyer"))
        val transaction = supportFragmentManager.beginTransaction()
        FragmentSmallHolder().let {
            transaction.replace(R.id.frame_layout, it)
        }
        transaction.commit()


        tab_layout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                if (p0?.position == 0) {
                    val transaction = supportFragmentManager.beginTransaction()
                    FragmentSmallHolder()
                        .let {
                            transaction.replace(R.id.frame_layout, it)
                        }
                    transaction.commit()
                } else {
                    val transaction = supportFragmentManager.beginTransaction()
                    BuyerFragment()
                        .let {
                            transaction.replace(R.id.frame_layout, it)
                        }
                    transaction.commit()
                }
            }

        })
    }
}