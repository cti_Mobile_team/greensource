package com.greenresource.greensourceemployee

import android.Manifest
import android.annotation.TargetApi
import android.content.ContentUris
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.greenresource.R
import com.greenresource.greensourceemployee.adapter.AdapterNotification
import com.greenresource.model.ModelLogin
import com.greenresource.model.ModelNotification
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.utils.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.navigationview_header.*
import kotlinx.android.synthetic.main.toolbar_back.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.email
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditProfileActivity : AppCompatActivity() {
    val REQUEST_PERMISSION = 100
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_PICK_IMAGE = 2
    var customProgressDialog: CustomDialogProgress? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        tv_title.text = getString(R.string.profile)
        customProgressDialog = CustomDialogProgress(this)
        input_first_name.setText(DataManager.getInstance().getUserInfo(this).userinfo.firstName)
        input_last_name.setText(DataManager.getInstance().getUserInfo(this).userinfo.lastName)
        input_email.setText(DataManager.getInstance().getUserInfo(this).userinfo.email)

        imageView.setOnClickListener {
            checkCameraPermission()
        }

        image_back.setOnClickListener {
            onBackPressed()
        }

        btn_sign_up.setOnClickListener {
            getEditProfile()
        }


    }


    fun editProfilePicture(path: String) {
        customProgressDialog?.show()
        val mediaType = MediaType.parse("text/plain")
        var partimage: MultipartBody.Part =
            BackEndApi.prepareFilePart(
                getApplication(),
                "profile_image",
                path
            )
        var email: RequestBody =
            MultipartBody.create(mediaType, DataManager.getInstance().getUserInfo(this).userinfo.id)
        WebServiceClient.client.create(BackEndApi::class.java)
            .editProfileImage(partimage, email).enqueue(object : Callback<ModelLogin> {
                override fun onFailure(call: Call<ModelLogin>?, t: Throwable?) {
                    customProgressDialog?.dismiss()
                    Log.e("TAG", t!!.message)
                }

                override fun onResponse(call: Call<ModelLogin>?, response: Response<ModelLogin>?) {
                    customProgressDialog?.dismiss()
                    if (response!!.code() == 200) {
                        if (response!!.body().success == "1") {
                            var response: String = Gson().toJson(response!!.body())
                            SessionManager.writeString(
                                this@EditProfileActivity,
                                Constant.userInfo,
                                response
                            )
                        }

                    }
                }

            })
    }


    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_PERMISSION
            )
        } else {
            chooseOptionForImages()
        }
    }

    private fun chooseOptionForImages() {
        val options =
            arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Choose your profile picture")
        builder.setItems(options, DialogInterface.OnClickListener { dialog, item ->
            when {
                options[item] == "Take Photo" -> {
                    openCamera()
                }
                options[item] == "Choose from Gallery" -> {
                    openGallery()
                }
                options[item] == "Cancel" -> {
                    dialog.dismiss()
                }
            }
        })
        builder.show()
    }

    private fun openGallery() {
        val intent = Intent("android.intent.action.GET_CONTENT")
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_PICK_IMAGE)
    }


    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
            intent.resolveActivity(packageManager)?.also {
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }


    fun getEditProfile() {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["first_name"] = input_first_name!!.text!!.toString()
        map["last_name"] = input_last_name!!.text!!.toString()
        map["email"] = input_email!!.text!!.toString()
        map["user_id"] = DataManager.getInstance().getUserInfo(this).userinfo.id
        WebServiceClient.client.create(BackEndApi::class.java).editProfile(map)
            .enqueue(
                object : Callback<ModelLogin> {
                    override fun onFailure(call: Call<ModelLogin>?, t: Throwable?) {
                        customProgressDialog?.dismiss()
                    }

                    override fun onResponse(
                        call: Call<ModelLogin>?,
                        response: Response<ModelLogin>?
                    ) {
                        customProgressDialog?.dismiss()
                        if (response!!.code() == 200) {
                            if (response!!.body().success == "1") {
                                var response: String = Gson().toJson(response!!.body())
                                SessionManager.writeString(
                                    this@EditProfileActivity,
                                    Constant.userInfo,
                                    response
                                )
                            }

                        }
                    }

                }
            )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size!! > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                chooseOptionForImages()
            }
        }
    }

    private fun getPath(data: Intent?): String? {
        if (data != null) {
            val bundle = data!!.extras
            val imagebitmap = bundle!!["data"] as Bitmap?
            val path = MediaStore.Images.Media.insertImage(
                contentResolver,
                imagebitmap,
                "IMG_" + System.currentTimeMillis(),
                null
            )
            Log.e("TAG", path)
            var photoPath = FileUtils.getPath(this, Uri.parse(path))
            return photoPath
        }
        return ""

    }

    @TargetApi(19)
    private fun handleImageOnKitkat(data: Intent?) {
        var imagePath: String? = null
        val uri = data!!.data
        //DocumentsContract defines the contract between a documents provider and the platform.
        if (DocumentsContract.isDocumentUri(this, uri)) {
            val docId = DocumentsContract.getDocumentId(uri)
            if (uri != null) {
                if ("com.android.providers.media.documents" == uri.authority) {
                    val id = docId.split(":")[1]
                    val selsetion = MediaStore.Images.Media._ID + "=" + id
                    imagePath = getImagePath(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        selsetion
                    )
                } else if ("com.android.providers.downloads.documents" == uri.authority) {
                    val contentUri = ContentUris.withAppendedId(
                        Uri.parse(
                            "content://downloads/public_downloads"
                        ), java.lang.Long.valueOf(docId)
                    )
                    imagePath = getImagePath(contentUri, null)
                }
            }
        } else if ("content".equals(uri!!.scheme, ignoreCase = true)) {
            imagePath = getImagePath(uri, null)
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            imagePath = uri.path
        }
        renderImage(imagePath)
    }

    private fun getImagePath(uri: Uri?, selection: String?): String {
        var path: String? = null
        val cursor = uri?.let { contentResolver.query(it, null, selection, null, null) }
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
            }
            cursor.close()
        }
        editProfilePicture(path!!)
        return path!!
    }

    private fun renderImage(imagePath: String?) {
        if (imagePath != null) {
            val bitmap = BitmapFactory.decodeFile(imagePath)
            imageView?.setImageBitmap(bitmap)
        } else {
            // show("ImagePath is null")
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                val bitmap = data?.extras?.get("data") as Bitmap
                imageView.setImageBitmap(bitmap)
                var photoPath = getPath(data)
                editProfilePicture(photoPath!!)
            } else if (requestCode == REQUEST_PICK_IMAGE) {
                val uri = data?.getData()
                handleImageOnKitkat(data)
            }
        }
    }
}