package com.greenresource.greensourceemployee.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.greenresource.R
import com.greenresource.greensourceemployee.adapter.AdapterSmallHolders
import com.greenresource.model.ModelBuyerList
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.utils.DataManager
import kotlinx.android.synthetic.main.fragment_buyer.*
import kotlinx.android.synthetic.main.fragment_small_holder.*
import kotlinx.android.synthetic.main.fragment_small_holder.tv_no_data_found
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
var customProgressDialog: CustomDialogProgress? = null

class FragmentSmallHolder : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_small_holder, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // rv_Small_holder_list.adapter = AdapterSmallHolders(context!!)
        customProgressDialog = CustomDialogProgress(activity!!)
        getallSellPalmOilRequest()
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentSmallHolder().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun getallSellPalmOilRequest() {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["user_id"] = DataManager.getInstance().getUserInfo(activity).userinfo.id
        WebServiceClient.client.create(BackEndApi::class.java).allSellPalmOilRequest(map)
            .enqueue(
                object : Callback<ModelBuyerList> {
                    override fun onFailure(call: Call<ModelBuyerList>?, t: Throwable?) {
                        customProgressDialog?.dismiss()
                    }

                    override fun onResponse(
                        call: Call<ModelBuyerList>?,
                        response: Response<ModelBuyerList>?
                    ) {
                        customProgressDialog?.dismiss()
                        if (response?.body()?.success == "1") {
                            if (response?.body()?.buyersList?.size!! > 0) {
                                tv_no_data_found.visibility = View.GONE
                                rv_Small_holder_list.adapter = AdapterSmallHolders(
                                    context!!,
                                    response?.body()?.buyersList as ArrayList<ModelBuyerList.BuyersList>
                                )
                            } else {
                                tv_no_data_found.visibility = View.VISIBLE
                                Toast.makeText(
                                    activity!!,
                                    response?.body()?.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            tv_no_data_found.visibility = View.VISIBLE
                            Toast.makeText(
                                activity!!,
                                response?.body()?.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }


                }
            )
    }
}