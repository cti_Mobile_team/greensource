package com.greenresource.greensourceemployee

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.greenresource.R
import com.greenresource.greensourceemployee.adapter.AdapterNotification
import com.greenresource.model.ModelNotification
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.utils.DataManager
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.toolbar_back.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity : AppCompatActivity() {
    var customProgressDialog: CustomDialogProgress? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        customProgressDialog = CustomDialogProgress(this)
        image_search.visibility = View.GONE
        tv_title.text = getString(R.string.notification)

        image_back.setOnClickListener {
            onBackPressed()
        }

        getNotificationList()
        image_search.setOnQueryTextFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                tv_title.visibility = View.GONE
            } else {
                tv_title.visibility = View.VISIBLE
            }
        }

    }


    fun getNotificationList() {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["user_id"] = DataManager.getInstance().getUserInfo(this).userinfo.id
        WebServiceClient.client.create(BackEndApi::class.java).notificationHistory(map)
            .enqueue(
                object : Callback<ModelNotification> {
                    override fun onFailure(call: Call<ModelNotification>?, t: Throwable?) {
                        customProgressDialog?.dismiss()
                    }

                    override fun onResponse(
                        call: Call<ModelNotification>?,
                        response: Response<ModelNotification>?
                    ) {
                        customProgressDialog?.dismiss()
                        if (response!!.code() == 200) {
                            if (response!!.body().success == "1") {
                                if (response!!.body()!!.notificationList.size > 0) {
                                    tv_no_data_found.visibility = View.GONE
                                    rv_notification.adapter = AdapterNotification(
                                        this@NotificationActivity,
                                        response!!.body()!!.notificationList as ArrayList<ModelNotification.NotificationList>
                                    )
                                } else {
                                    tv_no_data_found.visibility = View.VISIBLE
                                }
                            } else {
                                tv_no_data_found.visibility = View.VISIBLE
                            }

                        }
                    }

                }
            )
    }
}