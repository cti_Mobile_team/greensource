package com.greenresource.greensourceemployee

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.greenresource.R
import com.greenresource.databinding.ActivitySmallHoldersBinding
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.viewmodel.AddSmallHolderViewModel
import com.greenresource.viewmodel.SignUpViewModel
import kotlinx.android.synthetic.main.activity_small_holders.*
import java.util.*

class AddSmallHoldersActivity : AppCompatActivity(), OnMapReadyCallback {
    var customProgressDialog: CustomDialogProgress? = null
    var addSmallHolderViewModel: AddSmallHolderViewModel? = null
    private lateinit var mMap: GoogleMap
    val PERMISSIONID = 123
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    val PERMISSION_ID = 42
    var activitySmallHoldersBinding: ActivitySmallHoldersBinding? = null
    var userAddress = ""
    var userLatitude = ""
    var userLongitude = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySmallHoldersBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_small_holders)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        customProgressDialog = CustomDialogProgress(this)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        addSmallHolderViewModel =
            ViewModelProviders.of(this).get(AddSmallHolderViewModel::class.java)
        activitySmallHoldersBinding?.addSmallHolderView = addSmallHolderViewModel

        addSmallHolderViewModel?.progressDialog?.observe(this, androidx.lifecycle.Observer {
            if (it) customProgressDialog?.show() else customProgressDialog?.dismiss()
        })

        addSmallHolderViewModel?.modelSignUp?.observe(
            this,
            androidx.lifecycle.Observer { modelSignUp ->
                if (modelSignUp?.success == "1") {
                    Toast.makeText(this, modelSignUp?.message, Toast.LENGTH_SHORT).show()
                    finish()
                }
            })

        btn_save?.setOnClickListener { addSmallHolderViewModel?.onAddSmallHolder(this) }
        img_back.setOnClickListener { onBackPressed() }

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        getLastLocation()
        mMap.setOnCameraIdleListener {
            val midLatLng: LatLng = googleMap.cameraPosition.target
            tv_location
                .text = getAddress(
                midLatLng.latitude,
                midLatLng.longitude
            )
        }
    }


    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), PERMISSIONID
        )
    }

    fun getAddress(latitude: Double?, longitude: Double?): String? {
        Log.e("TAG", "Get last Known Location")
        var address: String? = ""
        try {
            val addresses: List<Address>
            val geocoder = Geocoder(this, Locale.getDefault())
            addresses = geocoder.getFromLocation(latitude!!, longitude!!, 1)
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses[0].getAddressLine(0)
            val city: String = addresses[0].getLocality()
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            /*
              val state: String = addresses[0].getAdminArea()
              val country: String = addresses[0].getCountryName()
              val postalCode: String = addresses[0].getPostalCode()
              val knownName: String = addresses[0].getFeatureName()*/
        } catch (e: Exception) {
            e.printStackTrace()
            getLastLocation()
        }
        tv_location.text = address
        userAddress = address!!
        addSmallHolderViewModel?.onSetLocation(userAddress, userLatitude, userLongitude)
        return address
    }


    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            mMap.clear()
            userLatitude = mLastLocation?.latitude.toString()
            userLongitude = mLastLocation?.longitude.toString()
            addmarker(mLastLocation?.latitude, mLastLocation?.longitude!!)
            tv_location.text = getAddress(
                mLastLocation?.latitude, mLastLocation?.longitude!!
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }


    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        mMap.clear()
                        addmarker(location.latitude, location.longitude)
                        userLatitude = location.latitude.toString()
                        userLongitude = location.longitude.toString()
                        tv_location.text = getAddress(
                            location?.latitude, location?.longitude!!
                        )
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }


    private fun addmarker(latitude: Double, longitude: Double) {
        mMap.clear()
        /*   mMap.addMarker(
               markerOptions.position(
                   LatLng(
                       latitude,
                       longitude
                   )
               ).icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_location))
           )*/
        mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    latitude,
                    longitude
                ), 16f
            )
        )
    }
}