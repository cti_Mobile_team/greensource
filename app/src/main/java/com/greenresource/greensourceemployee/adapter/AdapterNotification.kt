package com.greenresource.greensourceemployee.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.greenresource.R
import com.greenresource.model.ModelNotification
import java.text.SimpleDateFormat

class AdapterNotification : RecyclerView.Adapter<AdapterNotification.MyViewHolder> {
    var context: Context? = null
    var arrayList: ArrayList<ModelNotification.NotificationList>? = null

    constructor(context: Context, arrayList: ArrayList<ModelNotification.NotificationList>) {
        this.context = context
        this.arrayList = arrayList
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_notification: TextView = itemView.findViewById(R.id.tv_notification)
        var tv_date: TextView = itemView.findViewById(R.id.tv_date)
        var rv_request: TextView = itemView.findViewById(R.id.rv_request)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tv_notification.text = arrayList!!.get(position)!!.message
        var date = SimpleDateFormat("yyyy-mm-dd hh:mm:ss")
        var datec = SimpleDateFormat("dd MMM yyyy")
        var datechange = datec.format(date.parse(arrayList!!.get(position)!!.createdAt))
        holder.tv_date.text = datechange
        holder.rv_request.text = arrayList!![position]!!.type
    }
}