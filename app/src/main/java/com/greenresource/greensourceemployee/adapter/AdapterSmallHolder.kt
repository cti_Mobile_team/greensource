package com.greenresource.greensourceemployee.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.greenresource.R
import com.greenresource.model.ModelSmallHolderList

class AdapterSmallHolder : RecyclerView.Adapter<AdapterSmallHolder.MyViewHolder> {
    var context: Context? = null
    var arrayList: ArrayList<ModelSmallHolderList.Smallholder>? = null

    constructor(context: Context, arrayList: ArrayList<ModelSmallHolderList.Smallholder>) {
        this.context = context
        this.arrayList = arrayList
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rv_Small_holder_name: TextView = itemView.findViewById(R.id.rv_Small_holder_name)
        var rv_Small_holder_location: TextView =
            itemView.findViewById(R.id.rv_Small_holder_location)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_small_holder, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder?.rv_Small_holder_name?.text =
            arrayList?.get(position)?.firstName + " " + arrayList?.get(position)?.lastName
        holder?.rv_Small_holder_location?.text = arrayList?.get(position)?.userAddress
    }
}