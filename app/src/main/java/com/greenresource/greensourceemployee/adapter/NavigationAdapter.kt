package com.greenresource.greensourceemployee.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.greenresource.R
import com.greenresource.greensourceemployee.*
import com.greenresource.utils.Constant
import com.greenresource.utils.DataManager
import com.greenresource.utils.SessionManager

class NavigationAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder> {
    var arrayList: ArrayList<String>? = null
    var context: Context? = null
    var userType: String = ""
    var setOnNavigationItemClickListener: SetOnNavigationItemClickListener? = null

    constructor(
        context: Context,
        arrayList: ArrayList<String>,
        userType: String,
        setOnNavigationItemClickListener: SetOnNavigationItemClickListener
    ) {
        this.context = context
        this.arrayList = arrayList
        this.userType = userType
        this.setOnNavigationItemClickListener = setOnNavigationItemClickListener
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_title: TextView = itemView.findViewById(R.id.tv_title)

    }

    class MyHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageUser: ImageView = itemView.findViewById(R.id.imageUser)
        var tv_user_name: TextView = itemView.findViewById(R.id.tv_user_name)
        var tv_user_email: TextView = itemView.findViewById(R.id.tv_user_email)
        var textEdit: TextView = itemView.findViewById(R.id.textEdit)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var itemView: View
        var holder: RecyclerView.ViewHolder
        if (viewType == 0) {
            itemView =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.navigationview_header, parent, false)
            holder = MyHeaderViewHolder(itemView)
        } else {
            itemView =
                LayoutInflater.from(parent.context).inflate(R.layout.item_navigation, parent, false)
            holder = MyViewHolder(itemView)
        }

        return holder
    }

    override fun getItemCount(): Int {
        return arrayList?.size!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0) {
            (holder as MyHeaderViewHolder).tv_user_name.text = DataManager.getInstance()
                .getUserInfo(context).userinfo.firstName + " " + DataManager.getInstance()
                .getUserInfo(context).userinfo.lastName
            (holder as MyHeaderViewHolder).tv_user_email.text =
                DataManager.getInstance().getUserInfo(context).userinfo.email
            Glide.with(context!!)
                .load(
                    Constant.ImageBase + DataManager.getInstance()
                        .getUserInfo(context).userinfo.profileImage
                )
                .apply(RequestOptions().error(R.mipmap.img1).placeholder(R.mipmap.img1))
                .into((holder as MyHeaderViewHolder).imageUser)
            (holder as MyHeaderViewHolder).textEdit.setOnClickListener {
                context?.startActivity(Intent(context, EditProfileActivity::class.java))
            }
        } else {
            (holder as MyViewHolder).tv_title.text = arrayList?.get(position)
            holder.itemView.setOnClickListener {
                when ((holder as MyViewHolder).tv_title.text.toString()) {
                    "Add Smallholder" -> {
                        context?.startActivity(Intent(context, SmallHolderListActivity::class.java))
                    }
                    "Palm Oil Requests" -> {
                        context?.startActivity(Intent(context, PalmOilRequestList::class.java))
                    }
                    "Rent Machine" -> {
                        if (userType == "1") {
                            context?.startActivity(Intent(context, RentMachineActivity::class.java))
                        } else if (userType == "2") {
                            setOnNavigationItemClickListener?.onNavigationItemClick("Rent machine")
                        }
                    }
                    "Sell Palm Oil" -> {
                        setOnNavigationItemClickListener?.onNavigationItemClick("Sell Palm Oil")
                    }
                    "Purchase Palm Oil" -> {
                        setOnNavigationItemClickListener?.onNavigationItemClick("Purchase Palm Oil")
                    }
                    "Notification" -> {
                        context?.startActivity(Intent(context, NotificationActivity::class.java))
                    }
                    "Settings" -> {
                        context?.startActivity(Intent(context, SettingActivity::class.java))
                    }
                    "Support" -> {

                    }
                    "Log out" -> {
                        SessionManager.clear(context!!)
                    }
                }
            }
        }


    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface SetOnNavigationItemClickListener {
        fun onNavigationItemClick(value: String)
    }
}