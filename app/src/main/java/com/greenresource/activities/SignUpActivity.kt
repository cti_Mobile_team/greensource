package com.greenresource.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.*
import com.greenresource.R
import com.greenresource.databinding.ActivitySignUpBinding
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.viewmodel.SignUpViewModel
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.util.*

class SignUpActivity : AppCompatActivity() {
    var customProgressDialog: CustomDialogProgress? = null
    var signViewModel: SignUpViewModel? = null
    var signUpBinding: ActivitySignUpBinding? = null
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    val PERMISSIONID = 123
    var userAddress = ""
    var userLatitude = ""
    var userLongitude = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        signUpBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        customProgressDialog = CustomDialogProgress(this)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        signViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        signUpBinding?.signUpViewModel = signViewModel

        signViewModel?.progressDialog?.observe(this, Observer {
            if (it) customProgressDialog?.show() else customProgressDialog?.dismiss()
        })

        signViewModel?.modelSignUp?.observe(this, Observer { modelSignUp ->
            if (modelSignUp != null) {
                if (modelSignUp?.success == "1") {
                    Toast.makeText(this, modelSignUp?.message, Toast.LENGTH_SHORT).show()
                    startActivity(
                        Intent(
                            this@SignUpActivity,
                            LoginActivity::class.java
                        )
                    )
                } else {
                    Toast.makeText(this, modelSignUp?.message, Toast.LENGTH_SHORT).show()
                }
            }
        })


        val userType = resources.getStringArray(R.array.type_array)
        spinner.adapter = ArrayAdapter(
            this,
            android.R.layout.simple_dropdown_item_1line
            , userType
        )
        btn_sign_up.setOnClickListener {
            signViewModel?.onSignUp()

        }
        getLastLocation()

    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), PERMISSIONID
        )
    }


    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        getAddress(location?.latitude, location?.longitude!!)
                        userLatitude = "" + location?.latitude
                        userLongitude = "" + location?.longitude

                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            getAddress(mLastLocation?.latitude, mLastLocation?.longitude!!)
            userLatitude = "" + mLastLocation?.latitude
            userLongitude = "" + mLastLocation?.longitude
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    fun getAddress(latitude: Double?, longitude: Double?): String? {
        Log.e("TAG", "Get last Known Location")
        var address: String? = ""
        try {
            val addresses: List<Address>
            val geocoder = Geocoder(this, Locale.getDefault())
            addresses = geocoder.getFromLocation(latitude!!, longitude!!, 1)
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses[0].getAddressLine(0)
            val city: String = addresses[0].getLocality()
            userAddress = address!!
            signViewModel?.onSetLocation(userAddress, latitude.toString(), longitude.toString())
            Log.e("TAG", userAddress + latitude.toString() + longitude.toString())
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            /*
              val state: String = addresses[0].getAdminArea()
              val country: String = addresses[0].getCountryName()
              val postalCode: String = addresses[0].getPostalCode()
              val knownName: String = addresses[0].getFeatureName()*/
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("TAG Location", e.message)
            // getLastLocation()
        }
        return address
    }
}