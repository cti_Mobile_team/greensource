package com.greenresource.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.greenresource.MainActivity
import com.greenresource.R
import com.greenresource.utils.DataManager

class SplashActivity : AppCompatActivity() {
    private var SPLASH_TIME_OUT = 3000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()
        Handler().postDelayed(Runnable {
            if (DataManager.getInstance()
                    .getUserInfo(this) != null && DataManager.getInstance()
                    .getUserInfo(this).userinfo.id != null && DataManager.getInstance()
                    .getUserInfo(this).userinfo.id != ""
            ) {
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
            }

        }, SPLASH_TIME_OUT.toLong())
    }
}