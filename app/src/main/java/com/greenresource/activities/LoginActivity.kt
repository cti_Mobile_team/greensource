package com.greenresource.activities

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.greenresource.MainActivity
import com.greenresource.R
import com.greenresource.databinding.ActivityLoginBinding
import com.greenresource.model.ModelLogin
import com.greenresource.retrofit.BackEndApi
import com.greenresource.retrofit.WebServiceClient
import com.greenresource.smallholders.dialog.DialogEntry
import com.greenresource.utils.Constant
import com.greenresource.utils.CustomDialogProgress
import com.greenresource.utils.SessionManager
import com.greenresource.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity(), DialogEntry.SetonItemSelected {
    var activityLoginBinding: ActivityLoginBinding? = null
    var customProgressDialog: CustomDialogProgress? = null
    var loginViewModel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        activityLoginBinding?.loginViewModel = loginViewModel
        customProgressDialog = CustomDialogProgress(this)


        loginViewModel?.progressDialog?.observe(this, Observer {
            if (it) customProgressDialog?.show() else customProgressDialog?.dismiss()
        })

        loginViewModel?.modelLogin?.observe(this, Observer { modelLogin ->
            if (modelLogin?.success == "1") {
                var response: String = Gson().toJson(modelLogin)
                SessionManager.writeString(this, Constant.userInfo, response)
                Toast.makeText(this, modelLogin?.message, Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                Toast.makeText(this, modelLogin?.message, Toast.LENGTH_SHORT).show()
            }

        })


        tv_sign_up.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }
        tv_forgot_pass.setOnClickListener {
            DialogEntry(this, this).dialogWithInput("Forgot Password", "Enter Your Email")
        }
    }

    override fun onItemSelection(value: String, title: String) {
        forgotPassword(value)
    }

    fun forgotPassword(email: String) {
        customProgressDialog?.show()
        var map = HashMap<String, String>()
        map["email"] = email
        WebServiceClient?.client.create(BackEndApi::class.java).forgetPassword(map)
            .enqueue(object : Callback<ModelLogin> {
                override fun onFailure(call: Call<ModelLogin>?, t: Throwable?) {
                    customProgressDialog?.dismiss()
                }

                override fun onResponse(call: Call<ModelLogin>?, response: Response<ModelLogin>?) {
                    customProgressDialog?.dismiss()
                    Toast.makeText(
                        this@LoginActivity,
                        response?.body()?.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            })
    }
}