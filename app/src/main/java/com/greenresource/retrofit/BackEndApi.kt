package com.greenresource.retrofit

import android.content.Context
import android.net.Uri
import com.greenresource.model.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import java.io.File

interface BackEndApi {

    companion object {
        fun prepareFilePart(
            context: Context?,
            partName: String?,
            fileUripath: String
        ): MultipartBody.Part {
            return if (!fileUripath.isEmpty()) {
                val fileUri = Uri.fromFile(File(fileUripath))
                val file: File = FileUtils.getFile(context, fileUri)
                // create RequestBody instance from file
                val requestFile = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    file
                )
                MultipartBody.Part.createFormData(partName, file.name, requestFile)
            } else {
                val attachmentEmpty =
                    RequestBody.create(MediaType.parse("text/plain"), "")
                MultipartBody.Part.createFormData("attachment", "", attachmentEmpty)
            }
        }
    }

    // TODO: 2/26/2020 : TRADERS API SECTION:
    @FormUrlEncoded
    @POST("api/signup")
    fun signUp(@FieldMap map: HashMap<String, String>): Call<ModelSignUp>

    @FormUrlEncoded
    @POST("api/login")
    fun login(@FieldMap map: HashMap<String, String>): Call<ModelLogin>

    @FormUrlEncoded
    @POST("api/addSmallholders")
    fun addSmallholders(@FieldMap map: HashMap<String, String>): Call<ModelSignUp>

    @FormUrlEncoded
    @POST("api/get_all_smallholders")
    fun getAllSmallholders(@FieldMap map: HashMap<String, String>): Call<ModelSmallHolderList>

    @FormUrlEncoded
    @POST("api/sell_palm_oil_request")
    fun sellPalmOilRequest(@FieldMap map: HashMap<String, String>): Call<ModelSmallHolderList>

    @FormUrlEncoded
    @POST("api/buyer_palm_oil_request")
    fun palmOilRequest(@FieldMap map: HashMap<String, String>): Call<ModelSmallHolderList>

    @FormUrlEncoded
    @POST("api/rent_machine_request")
    fun rentMachineRequest(@FieldMap map: HashMap<String, String>): Call<ModelSmallHolderList>

    @FormUrlEncoded
    @POST("api/allbuyersRequestforpalmoil")
    fun allbuyersRequestforpalmoil(@FieldMap map: HashMap<String, String>): Call<ModelBuyerList>

    @FormUrlEncoded
    @POST("api/allsell_palm_oil_request")
    fun allSellPalmOilRequest(@FieldMap map: HashMap<String, String>): Call<ModelBuyerList>

    @FormUrlEncoded
    @POST("api/allrentmachine")
    fun allrentmachine(@FieldMap map: HashMap<String, String>): Call<ModelSmallHolderList>

    @FormUrlEncoded
    @POST("api/forgetPassword")
    fun forgetPassword(@FieldMap map: HashMap<String, String>): Call<ModelLogin>

    @FormUrlEncoded
    @POST("api/near_by_smallholders")
    fun nearBySmallholders(@FieldMap map: HashMap<String, String>): Call<ModelNearBy>

    @FormUrlEncoded
    @POST("api/notificationHistory")
    fun notificationHistory(@FieldMap map: HashMap<String, String>): Call<ModelNotification>


    @FormUrlEncoded
    @POST("api/userProfile")
    fun userProfile(@FieldMap map: HashMap<String, String>): Call<ModelNotification>

    @FormUrlEncoded
    @POST("api/editProfile")
    fun editProfile(@FieldMap map: HashMap<String, String>): Call<ModelLogin>


    @FormUrlEncoded
    @POST("api/update_notification")
    fun updateNotification(@FieldMap map: HashMap<String, String>): Call<ModelLogin>


    @Multipart
    @POST("api/edit_profile_image")
    fun editProfileImage(
        @Part image: MultipartBody.Part,
        @Part("user_id") device_type: RequestBody
    ): Call<ModelLogin>


}
