package com.greenresource.utils;


import android.content.Context;

import com.google.gson.Gson;
import com.greenresource.model.ModelLogin;

public class DataManager {
    private static final DataManager ourInstance = new DataManager();

    public static DataManager getInstance() {
        return ourInstance;
    }

    private DataManager() {
    }

    public ModelLogin getUserInfo(Context context) {
        ModelLogin modelLogin = new Gson().fromJson(SessionManager.Companion.readString(context,Constant.Companion.getUserInfo(),""),ModelLogin.class);
        return modelLogin;
    }


}
