package com.greenresource.utils

import android.text.format.DateUtils
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class util {
    companion object {
        fun isEmailValid(email: String): Boolean {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            return matcher.matches()
        }

        fun isDOBValid(date: String): Boolean {
            val c = Calendar.getInstance()

            val sdf = SimpleDateFormat("dd/MM/yyyy")
            val dte: Date = sdf.parse(date)

            if (Date().after(dte)) {
                return true
            }

            return false
        }

        fun isDayValid(day: Int, month: Int, year: Int): Boolean {
            val c = Calendar.getInstance()
            val currentYear = c.get(Calendar.YEAR)
            val currentMonth = c.get(Calendar.MONTH)
            val currentDate = c.get(Calendar.DAY_OF_MONTH)

            Log.e("TAG", "Current Date: " + currentDate + currentMonth + currentYear)

            if (year <= currentYear) {
                if (month <= 12) {
                    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                        if (day < 32) {
                            return true
                        }
                    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
                        if (day < 31) {
                            return true
                        }
                    } else if (month == 2) {
                        if (day < 29) {
                            return true
                        }
                    }
                }
            }
            return false
        }

        fun isDateValid(date: String): Boolean {
            val c = Calendar.getInstance()
            val sdf = SimpleDateFormat("dd/MMM/yyyy")
            val dte: Date = sdf.parse(date)
            val startDate: Long = dte.time
            if (DateUtils.isToday(startDate)) {
                return true
            } else if (Date().before(dte)) {
                return true
            }
            return false
        }

    }

}