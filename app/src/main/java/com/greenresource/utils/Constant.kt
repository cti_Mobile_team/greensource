package com.greenresource.utils

class Constant {
    companion object {
        var serverUrl: String = "https://ctinfotech.com/CTCC/"
        val BaseUrl: String = serverUrl + "green_source/"
        val ImageBase: String = "https://ctinfotech.com/CTCC/green_source/assets/profile/"
        val userInfo: String = "user_info"
        val employee: String = "1"
        val smallHolders: String = "2"
        val buyer: String = "3"
    }
}