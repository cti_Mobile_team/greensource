package com.greenresource.utils;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.greenresource.R;
import com.greenresource.model.ModelNearBy;

public class RenderClustering extends DefaultClusterRenderer<ModelNearBy.SmallholdersList> {
    /* IconGenerator iconGenerator;
     Bitmap icon;
     Cluster<MyItem> cluster;
 */
    public RenderClustering(Context context, GoogleMap map, ClusterManager<ModelNearBy.SmallholdersList> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(ModelNearBy.SmallholdersList item, MarkerOptions markerOptions) {
        //  if (item.getTitle().equalsIgnoreCase("farmer")) {
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.farmer));
       /* } else {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.machine));
        }*/

    }

    @Override
    protected void onBeforeClusterRendered(Cluster<ModelNearBy.SmallholdersList> cluster, MarkerOptions markerOptions) {
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.farmer));
        //  Bitmap venueCircle = VenueCircleFactory.createFromCluster(cluster);
        /*if (cluster.getTitle().equalsIgnoreCase("farmer")) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.farmer));
        } else {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.machine));
        }*/
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<ModelNearBy.SmallholdersList> cluster) {
        Log.e("TAG Size", "" + cluster.getSize());
        return cluster.getSize() > 1;
    }
}
