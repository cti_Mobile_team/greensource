package com.greenresource.utils;

import java.util.ArrayList;

public class MenuArrayList {

    public ArrayList<String> getEmployeeMenuList() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Add Smallholder");
        arrayList.add("Add Smallholder");
        arrayList.add("Palm Oil Requests");
        arrayList.add("Rent Machine");
        arrayList.add("Notification");
        arrayList.add("Settings");
        arrayList.add("Support");
        arrayList.add("Logout");
        return arrayList;
    }

    public ArrayList<String> getSmallHolderMenuList() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Rent Machine");
        arrayList.add("Rent Machine");
        arrayList.add("Sell Palm Oil");
        arrayList.add("Notification");
        arrayList.add("Settings");
        arrayList.add("Support");
        arrayList.add("Logout");
        return arrayList;
    }


    public ArrayList<String> getBuyerMenuList() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Purchase Palm Oil");
        arrayList.add("Purchase Palm Oil");
        arrayList.add("Notification");
        arrayList.add("Settings");
        arrayList.add("Support");
        arrayList.add("Logout");
        return arrayList;
    }
}
